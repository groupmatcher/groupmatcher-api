package org.groupmatcher.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.groupmatcher.api.entity.Person;
import org.groupmatcher.api.enumeration.Choice;
import org.groupmatcher.api.exception.AppException;
import org.groupmatcher.api.exception.Error;
import org.groupmatcher.api.repository.PersonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonServiceImplTest {

  PersonRepository personRepository;
  PersonService personService;

  @BeforeEach
  void setUp() {
    personRepository = mock(PersonRepository.class);
    personService = new PersonServiceImpl(personRepository);
  }

  @Test
  void findByUuid() {
    Person person = new Person();
    person.setUuid(UUID.randomUUID().toString());
    person.setDeleted(Choice.NO);
    person.setFailed(Choice.YES);
    person.setFbProfileId("FBPROFILEID");
    when(personRepository.findOne(person.getUuid())).thenReturn(person);

    Person result = personService.findByUuid(person.getUuid());

    assertEquals(person.getUuid(), result.getUuid());
    assertEquals(person.getDeleted(), result.getDeleted());
    assertEquals(person.getFailed(), result.getFailed());
    assertEquals(person.getFbProfileId(), result.getFbProfileId());
  }

  @Test
  void findByUuidThrowsWhenNull() {
    AppException result = assertThrows(AppException.class, () -> personService.findByUuid("MOCK"));
    assertEquals(result.getError(), Error.PERSON_NOT_FOUND);
  }

  @Test
  void getMatches() {
    String uuid = UUID.randomUUID().toString();

    personService.getMatches(uuid);

    verify(personRepository).findMatches(uuid);
  }

  @Test
  void getNoneThatMatchets() {
    String uuid = UUID.randomUUID().toString();

    personService.getNoneThatMatchets(uuid);

    verify(personRepository).findNoneThatMatches(uuid);
  }

  @Test
  void create() {
    Person person = new Person();
    when(personRepository.save(person)).thenAnswer(invocation -> {
      Person p1 = new Person();
      p1.setUuid(UUID.randomUUID().toString());
      return p1;
    });

    Person result = personService.create(person);

    assertNotNull(result);
    assertNotNull(result.getUuid());
    verify(personRepository).save(person);
  }

  @Test
  void createShouldFailIfUUIDPassed() {
    Person person = new Person();
    person.setUuid(UUID.randomUUID().toString());

    AppException result = assertThrows(AppException.class, () -> personService.create(person));
    assertEquals(result.getError(), Error.PERSON_WITH_UUID_CREATE);
  }

  @Test
  void update() {
    PersonService service = spy(personService);
    Person person = new Person();
    person.setUuid(UUID.randomUUID().toString());
    when(personRepository.findOne(person.getUuid())).thenReturn(person);

    service.update(person);

    verify(service).findByUuid(person.getUuid());
  }

  @Test
  void updateShouldFailIfNoUuid() {
    PersonService service = spy(personService);
    Person person = new Person();

    AppException result = assertThrows(AppException.class, () -> service.update(person));

    assertEquals(result.getError(), Error.PERSON_NOT_FOUND);
    verify(service).findByUuid(any());
  }

  @Test
  void delete() {
    PersonService service = spy(personService);
    Person person = new Person();
    person.setUuid(UUID.randomUUID().toString());
    when(personRepository.findOne(person.getUuid())).thenReturn(person);

    service.delete(person.getUuid());

    verify(personRepository).save(person);
    verify(service).findByUuid(person.getUuid());
    // Since i'm passing trough the mock the same instance of
    // this object, it's properties will be the same here
    assertEquals(Choice.YES, person.getDeleted());
  }

  @Test
  void deleteShouldFailIfNoFind() {
    PersonService service = spy(personService);
    String uuid = UUID.randomUUID().toString();

    AppException result = assertThrows(AppException.class, () -> service.delete(uuid));

    assertEquals(result.getError(), Error.PERSON_NOT_FOUND);
    verify(service).findByUuid(uuid);
  }

}