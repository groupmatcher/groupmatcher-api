package org.groupmatcher.api.exception;

import org.springframework.http.HttpStatus;

public enum Error {
  UNKWNOWN_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "0001"),
  PERSON_NOT_FOUND(HttpStatus.NOT_FOUND, "1001"),
  PERSON_WITH_UUID_CREATE(HttpStatus.BAD_REQUEST, "1002");

  private HttpStatus status;
  private String code;

  Error(HttpStatus status, String code) {
    this.status = status;
    this.code = code;
  }
}
