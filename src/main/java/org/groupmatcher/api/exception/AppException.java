package org.groupmatcher.api.exception;

public class AppException extends RuntimeException {
  private Error error;

  public AppException(Error error) {
    super(error.name());
    this.error = error;
  }

  public AppException(Error error, Throwable cause) {
    super(error.name(), cause);
    this.error = error;
  }

  public Error getError() {
    return error;
  }
}
