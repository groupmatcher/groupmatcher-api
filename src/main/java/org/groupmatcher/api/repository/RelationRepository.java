package org.groupmatcher.api.repository;

import org.groupmatcher.api.entity.Person;
import org.springframework.data.repository.CrudRepository;

public interface RelationRepository extends CrudRepository<Person, String> {
}
