package org.groupmatcher.api.repository;

import org.groupmatcher.api.entity.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, String> {

    Person findByFbProfileId(String fbProfileId);

    @Query(value = ""
            + "SELECT * "
            + "  FROM person p "
            + "  WHERE p.deleted = 'NO'"
            + "    AND p.is_in_group = 'YES'"
            + "    AND p.uuid not in ("
            + "    SELECT r.matched"
            + "      FROM relation r"
            + "      WHERE r.matchingDate = DATE(SYSDATE()))"
            + "    AND p.uuid not in ("
            + "    SELECT r.matches"
            + "      FROM relation r"
            + "      WHERE r.matchingDate = DATE(SYSDATE()))"
            + "  ORDER BY p.failed desc", nativeQuery = true)
    Person findNextPerson();

    @Query(value = ""
            + "SELECT * "
            + "  FROM person p"
            + "  WHERE p.deleted = 'NO'"
            + "    AND p.is_in_group = 'YES'"
            + "    AND p.uuid not in ("
            + "    SELECT r.matched"
            + "      FROM relation r"
            + "      WHERE r.matches = ?1)"
            + "    AND p.uuid not in ("
            + "    SELECT r.matches"
            + "      FROM relation r"
            + "      WHERE r.matched = ?1)", nativeQuery = true)
    List<Person> findNoneThatMatches(String personUuid);

    @Query(value = ""
            + "SELECT * "
            + "  FROM person p"
            + "  WHERE p.deleted = 'NO'"
            + "    AND p.is_in_group = 'YES'"
            + "    AND ( p.uuid in ("
            + "    SELECT r.matched"
            + "      FROM relation r"
            + "      WHERE r.matches = ?1)"
            + "    OR p.uuid in in ("
            + "    SELECT r.matches"
            + "      FROM relation r"
            + "      WHERE r.matched = ?1))", nativeQuery = true)
    List<Person> findMatches(String personUuid);
}
