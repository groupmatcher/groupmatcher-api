package org.groupmatcher.api.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "relation")
public class Relation {

  @Id @GeneratedValue(generator = "uuidGen")
  @GenericGenerator(name = "uuidGen", strategy = "uuid2")
  @Column(name = "uuid")
  private String uuid;

  @ManyToOne
  @Column(name = "matched")
  private Person matched;

  @ManyToOne
  @Column(name = "matches")
  private Person matches;

  @Column(name = "matchingDate")
  private ZonedDateTime matchingDate;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public Person getMatched() {
    return matched;
  }

  public void setMatched(Person matched) {
    this.matched = matched;
  }

  public Person getMatches() {
    return matches;
  }

  public void setMatches(Person matches) {
    this.matches = matches;
  }

  public ZonedDateTime getMatchingDate() {
    return matchingDate;
  }

  public void setMatchingDate(ZonedDateTime matchingDate) {
    this.matchingDate = matchingDate;
  }
}

