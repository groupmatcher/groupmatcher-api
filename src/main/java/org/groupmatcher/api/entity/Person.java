package org.groupmatcher.api.entity;

import org.groupmatcher.api.enumeration.Choice;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(generator = "uuidGen")
    @GenericGenerator(name = "uuidGen", strategy = "uuid2")
    @Column(name = "uuid")
    private String uuid;

    @Column(name = "fbProfileId", nullable = false, unique = true)
    private String fbProfileId;

    @Column(name = "failed", nullable = false)
    private Choice failed;

    @Column(name = "deleted", nullable = false)
    private Choice deleted;

    @Column(name = "is_in_group", nullable = false)
    private Choice isInGroup;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFbProfileId() {
        return fbProfileId;
    }

    public void setFbProfileId(String fbProfileId) {
        this.fbProfileId = fbProfileId;
    }

    public Choice getDeleted() {
        return deleted;
    }

    public void setDeleted(Choice deleted) {
        this.deleted = deleted;
    }

    public Choice getFailed() {
        return failed;
    }

    public void setFailed(Choice failed) {
        this.failed = failed;
    }
}
