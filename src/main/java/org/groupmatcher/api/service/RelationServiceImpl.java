package org.groupmatcher.api.service;

import java.time.ZonedDateTime;
import java.util.List;

import org.groupmatcher.api.entity.Person;
import org.groupmatcher.api.entity.Relation;
import org.springframework.stereotype.Service;

@Service
public class RelationServiceImpl implements RelationService {

  @Override
  public Relation relate(Person first, Person second) {
    return null;
  }

  @Override
  public List<Relation> findByMatchingDate(ZonedDateTime date) {
    return null;
  }

  @Override
  public List<Relation> getLastMatches() {
    return null;
  }
}
