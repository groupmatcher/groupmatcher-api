package org.groupmatcher.api.service;

import java.util.List;

import org.groupmatcher.api.entity.Person;
import org.groupmatcher.api.enumeration.Choice;
import org.groupmatcher.api.exception.AppException;
import org.groupmatcher.api.exception.Error;
import org.groupmatcher.api.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

  private PersonRepository personRepository;

  @Autowired
  public PersonServiceImpl(PersonRepository personRepository) {
    this.personRepository = personRepository;
  }

  @Override
  public Person findByUuid(String uuid) {
    Person person = personRepository.findOne(uuid);
    if (person == null) {
      throw new AppException(Error.PERSON_NOT_FOUND);
    }
    return person;
  }

  @Override
  public List<Person> getMatches(String uuid) {
    return personRepository.findMatches(uuid);
  }

  @Override
  public List<Person> getNoneThatMatchets(String uuid) {
    return personRepository.findNoneThatMatches(uuid);
  }

  @Override
  public Person getNextForMatch() {
    return personRepository.findNextPerson();
  }

  @Override
  public Person create(Person person) {
    if (person.getUuid() != null) {
      throw new AppException(Error.PERSON_WITH_UUID_CREATE);
    }

    return personRepository.save(person);
  }

  @Override
  public Person update(Person person) {
    findByUuid(person.getUuid());
    return personRepository.save(person);
  }

  @Override
  public void delete(String uuid) {
    Person person = findByUuid(uuid);
    person.setDeleted(Choice.YES);
    personRepository.save(person);
  }
}
