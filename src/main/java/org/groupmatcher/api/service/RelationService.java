package org.groupmatcher.api.service;

import java.time.ZonedDateTime;
import java.util.List;

import org.groupmatcher.api.entity.Person;
import org.groupmatcher.api.entity.Relation;

public interface RelationService {

  Relation relate(Person first, Person second);

  List<Relation> findByMatchingDate(ZonedDateTime date);

  List<Relation> getLastMatches();
}
