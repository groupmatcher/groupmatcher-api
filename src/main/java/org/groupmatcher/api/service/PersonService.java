package org.groupmatcher.api.service;

import java.util.List;

import org.groupmatcher.api.entity.Person;

public interface PersonService {

  Person findByUuid(String uuid);

  List<Person> getMatches(String uuid);

  List<Person> getNoneThatMatchets(String uuid);

  Person getNextForMatch();

  Person create(Person person);

  Person update(Person person);

  void delete(String uuid);
}
