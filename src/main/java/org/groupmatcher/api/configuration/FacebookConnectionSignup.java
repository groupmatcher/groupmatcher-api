package org.groupmatcher.api.configuration;

import static org.groupmatcher.api.enumeration.Choice.NO;

import org.groupmatcher.api.entity.Person;
import org.groupmatcher.api.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Service;

@Service
public class FacebookConnectionSignup implements ConnectionSignUp {

  @Autowired
  private PersonRepository personRepo;

  @Override
  public String execute(Connection<?> connection) {
    Connection<Facebook> fbC = (Connection<Facebook>) connection;
    Person p = new Person();
    p.setFailed(NO);
    p.setDeleted(NO);
    p.setFbProfileId(fbC.getApi().userOperations().getUserProfile().getId());
    p = personRepo.save(p);
    return p.getUuid();
  }
}
