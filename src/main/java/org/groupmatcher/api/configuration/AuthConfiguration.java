package org.groupmatcher.api.configuration;

import org.groupmatcher.api.configuration.properties.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInController;

@EnableWebSecurity
@Configuration
public class AuthConfiguration extends WebSecurityConfigurerAdapter {

  private SecurityConfig securityConfig;
  private UserDetailsService userDetailsService;
  private ConnectionFactoryLocator connectionFactoryLocator;
  private UsersConnectionRepository usersConnectionRepository;
  private FacebookConnectionSignup facebookConnectionSignup;

  @Autowired
  public AuthConfiguration(SecurityConfig securityConfig, UserDetailsService userDetailsService,
      ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository usersConnectionRepository,
      FacebookConnectionSignup facebookConnectionSignup) {
    this.securityConfig = securityConfig;
    this.userDetailsService = userDetailsService;
    this.connectionFactoryLocator = connectionFactoryLocator;
    this.usersConnectionRepository = usersConnectionRepository;
    this.facebookConnectionSignup = facebookConnectionSignup;
  }

  @Bean
  @Override
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers("/secure/**").authenticated()
        .and().formLogin().loginPage("/login").permitAll().and()
        .httpBasic()
        .realmName(securityConfig.getRealm())
        .and()
        .csrf()
        .disable();

  }

  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    converter.setSigningKey(securityConfig.getSigningKey());
    return converter;
  }

  @Bean
  public TokenStore tokenStore() {
    return new JwtTokenStore(accessTokenConverter());
  }

  @Bean
  public DefaultTokenServices tokenServices() {
    DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
    defaultTokenServices.setTokenStore(tokenStore());
    defaultTokenServices.setSupportRefreshToken(true);
    return defaultTokenServices;
  }

  @Bean
  public ProviderSignInController providerSignInController() {
    ((InMemoryUsersConnectionRepository) usersConnectionRepository)
        .setConnectionSignUp(facebookConnectionSignup);

    return new ProviderSignInController(
        connectionFactoryLocator,
        usersConnectionRepository,
        new FacebookSignInAdapter());
  }
}
