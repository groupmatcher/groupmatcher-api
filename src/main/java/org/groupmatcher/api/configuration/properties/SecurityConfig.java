package org.groupmatcher.api.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "security")
public class SecurityConfig {
  private String signingKey;
  private Integer encodingStrength;
  private String realm;

  public String getSigningKey() {
    return signingKey;
  }

  public void setSigningKey(String signingKey) {
    this.signingKey = signingKey;
  }

  public Integer getEncodingStrength() {
    return encodingStrength;
  }

  public void setEncodingStrength(Integer encodingStrength) {
    this.encodingStrength = encodingStrength;
  }

  public String getRealm() {
    return realm;
  }

  public void setRealm(String realm) {
    this.realm = realm;
  }
}
