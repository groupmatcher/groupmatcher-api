package org.groupmatcher.api.dto;

import static org.groupmatcher.api.enumeration.Choice.NO;

import java.util.Collection;

import org.groupmatcher.api.entity.Person;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;

public class UserDTO extends Person implements UserDetails {
  private String displayName;
  private String proPicUrl;

  public UserDTO(Person p, Connection<Facebook> fbUser) {
    this.displayName = fbUser.getDisplayName();
    this.proPicUrl = fbUser.getImageUrl();
    this.setDeleted(p.getDeleted());
    this.setFailed(p.getFailed());
    this.setFbProfileId(p.getFbProfileId());
    this.setUuid(p.getUuid());
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }

  @Override
  public String getPassword() {
    return null;
  }

  @Override
  public String getUsername() {
    return getFbProfileId();
  }

  @Override
  public boolean isAccountNonExpired() {
    return getDeleted() == NO;
  }

  @Override
  public boolean isAccountNonLocked() {
    return getDeleted() == NO;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return getDeleted() == NO;
  }

  @Override
  public boolean isEnabled() {
    return getDeleted() == NO;
  }
}
