package org.groupmatcher.api.worker.job;

import org.groupmatcher.api.service.PersonService;
import org.groupmatcher.api.service.RelationService;

public abstract class Job implements Runnable {
  protected PersonService personService;
  protected RelationService relationService;

  public Job(PersonService personService, RelationService relationService) {
    this.personService = personService;
    this.relationService = relationService;
  }
}
