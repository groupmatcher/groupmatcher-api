package org.groupmatcher.api.worker.job;

import org.groupmatcher.api.entity.Person;
import org.groupmatcher.api.enumeration.Choice;
import org.groupmatcher.api.service.PersonService;
import org.groupmatcher.api.service.RelationService;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

public class Match extends Job {

  public Match(PersonService personService, RelationService relationService) {
    super(personService, relationService);
  }

  @Override
  public void run() {
    Person person = null;
    while ((person = personService.getNextForMatch()) != null) {
      List<Person> noneThatMatches = personService.getNoneThatMatchets(person.getUuid());
      int elementNumber = noneThatMatches.size() - 1, match = 0;
      if (elementNumber < 0) {
        person.setFailed(Choice.YES);
        personService.update(person);
      } else {
        if (elementNumber > 0) {
          Random rand = new SecureRandom();
          match = rand.nextInt(elementNumber + 1);
        }
        relationService.relate(person, noneThatMatches.get(match));
      }
    }
  }
}
