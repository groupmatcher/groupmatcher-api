package org.groupmatcher.api.worker;

import org.groupmatcher.api.service.PersonService;
import org.groupmatcher.api.service.RelationService;
import org.groupmatcher.api.worker.job.Job;
import org.groupmatcher.api.worker.job.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class Worker implements Runnable {

  Class<? extends Job>[] jobs = new Class[]{
      Match.class
  };
  private PersonService personService;
  private RelationService relationService;

  @Autowired
  public Worker(PersonService personService, RelationService relationService) {
    this.personService = personService;
    this.relationService = relationService;
  }

  @Override
  @Scheduled(cron = "${groupmatcher.job.matchChron:0 9 * * MON}")
  public void run() {
    Arrays.stream(jobs).map(j -> {
          try {
            return j.getConstructor(PersonService.class, RelationService.class).newInstance(personService, relationService);
          } catch (Exception ex) {
            throw new RuntimeException(ex);
          }
        }
    ).forEach(Job::run);
  }
}
