CREATE TABLE `person` (
  `uuid` varchar(38) NOT NULL,
  `fbProfileId` varchar(255) UNIQUE NOT NULL DEFAULT '',
  `deleted` varchar(3) NOT NULL DEFAULT 'NO',
  `failed` varchar(3) NOT NULL DEFAULT 'NO',
  `is_in_group` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `relation` (
  `uuid` varchar(38) NOT NULL,
  `matched` varchar(38) NOT NULL,
  `matches` varchar(38) NOT NULL,
  `matchingDate` DATETIME NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY  `FK_MATCHED_PERSON_ID` (`matched`),
  KEY  `FK_MATCHES_PERSON_ID` (`matches`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
